#!env/bin/python
import os
import sys

if sys.platform == 'wn32':
    pybabel = 'env\\Scripts\\pybabel'
else:
    pybabel = 'env/bin/pybabel'

if len(sys.argv) < 2:
    print("usage: translate.py init|update|compile")
    sys.exit(1)


if sys.argv[1] == 'init':

    if len(sys.argv) != 3:
        print("usage: translate.py init <language-code>")
        sys.exit(1)

    os.system(pybabel + ' extract -F babel.cfg -k lazy_gettext -o messages.pot app')
    os.system(pybabel + ' init -i messages.pot -d app/translations -l ' + sys.argv[2])
    os.unlink('messages.pot')

elif sys.argv[1] == 'update':
    os.system(pybabel + ' extract -F babel.cfg -k lazy_gettext -o messages.pot app')
    os.system(pybabel + ' update -i messages.pot -d app/translations')
    os.unlink('messages.pot')

elif sys.argv[1] == 'compile':
    os.system(pybabel + ' compile -d app/translations')
