Sshmein.application_url = "/";

/*
Groups API tests
*/

module("Group's API", {

	setup: function() {
	
		$.mockjax({
			url: '/groups/new',
			responseTime: 0,
			contentType: 'text/json',
			response: function(settings) {
				var id = Math.floor(Math.random()*100+1);
				this.responseText = {description: "The group description", id: id , name: "newgroup_" + id};
			}
		});

		$.mockjax({
			url: '/groups/delete/*',
			responseText: 'none'
		});

		$.mockjax({
			url: '/groups/update',
			responseText: 'none'
		});

		Sshmein.collections.groups.init();
	},

	teardown: function() {
		var groups = Sshmein.collections.groups.getAllAsArray();
		var count = groups.length;
		for(var i=0; i<count; i++){
			Sshmein.collections.groups.remove(groups[i].id);
		}
	}
});

test('Create 3 groups', function(){
	stop();

	Sshmein.collections.groups.create();
	Sshmein.collections.groups.create();
	Sshmein.collections.groups.create();

	setTimeout(function(){
		var count = Sshmein.collections.groups.getAllAsArray().length;
		equal(count, 3, 'Expected 3 groups in collections');
		start();
	}, 600);
});

test('Create 3 groups, remove 1, remains 2', function(){
	stop();

	Sshmein.collections.groups.create();
	Sshmein.collections.groups.create();
	Sshmein.collections.groups.create();

	setTimeout(function(){
		//check count before
		var beforeCount = Sshmein.collections.groups.getAllAsArray().length;
		equal(beforeCount, 3, 'Before :: expected 3 groups in collections');

		//Remove 1 group
		var groupId = Sshmein.collections.groups.getAllAsArray()[0].id;
		Sshmein.collections.groups.remove(groupId);
		var afterCount = Sshmein.collections.groups.getAllAsArray().length;
		equal(afterCount, 2, 'After :: expected 2 groups in collections');

		start();
	}, 300);

});

test('Rename 1 group to "thegroup"', function(){
	stop();

	Sshmein.collections.groups.create();

	setTimeout(function(){
		var groupId = Sshmein.collections.groups.getAllAsArray()[0].id;
		
		//check name before
		var group = Sshmein.collections.groups.getOne(groupId);
		var currentGroupName = 'newgroup_' + groupId;
		equal(group.name, currentGroupName, 'Before :: expected the group name to be "' + currentGroupName + '"');
		
		//Update name
		Sshmein.collections.groups.update(groupId, 'name', 'thegroup');
		equal(group.name, 'thegroup', 'After :: expected the group name to be "thegroup"');

		start();
	}, 100);

});

test('Update group description for 1 group', function(){
	stop();

	Sshmein.collections.groups.create();

	setTimeout(function(){
		var groupId = Sshmein.collections.groups.getAllAsArray()[0].id;
		
		//check name before
		var group = Sshmein.collections.groups.getOne(groupId);
		var currentGroupDescription = 'The group description';
		equal(group.description, currentGroupDescription, 'Before :: expected the group description to be "' + currentGroupDescription + '"');
		
		//Update name
		Sshmein.collections.groups.update(groupId, 'description', 'The new group description');
		equal(group.description, 'The new group description', 'After :: expected the group description to be "The new group description"');
		
		start();
	}, 100);

});

test('Increment tunnel ON 2 times, decrement 1 time then to 0', function(){
	stop();

	setTimeout(function(){
		//Increment counter 2 times
		Sshmein.collections.groups.incrementTunnelOnCounters(45);
		Sshmein.collections.groups.incrementTunnelOnCounters(45);
		var	tunnelCounters = Sshmein.collections.groups.getTunnelOnCounters();
		equal(tunnelCounters[45], 2, "Expected group's tunnel ON to be 2");

		//Decrement counter
		Sshmein.collections.groups.decrementTunnelOnCounters(45);
		tunnelCounters = Sshmein.collections.groups.getTunnelOnCounters();
		equal(tunnelCounters[45], 1, "Expected group's tunnel ON to be 1");

		//Decrement again to 0
		Sshmein.collections.groups.decrementTunnelOnCounters(45);
		tunnelCounters = Sshmein.collections.groups.getTunnelOnCounters();
		equal(tunnelCounters[45], undefined, "Expected no more tunnel ON");
		
		start();
	}, 100);


});


/*
Hosts API tests
*/

module("Host's API",{
	setup: function() {

		Sshmein.collections.hosts.init();

		$.mockjax({
			url: '/hosts/new/*',
			responseTime: 0,
			contentType: 'text/json',
			response: function(settings) {
				var id = Math.floor(Math.random()*100+1);
				//console.log(id);
				var groupId = settings.url.match(/\/hosts\/new\/(.*)$/)[1];
				//console.log(groupId);
				this.responseText = {id: id , name: "host_" + id, group_id: groupId, tunnel_status:'on' };
			}
		});

		$.mockjax({
			url: '/hosts/delete/*',
			responseText: 'none'
		});

		$.mockjax({
			url: '/hosts/update',
			responseText: 'none'
		});

	},

	teardown: function() {
		var hosts = Sshmein.collections.hosts.getAllAsArray();
		var count = hosts.length;
		for(var i=0; i<count; i++){
			Sshmein.collections.hosts.remove(hosts[i].id);
		}
	}
});

test('Create 4 hosts in 2 differents groups', function(){
	stop();
	Sshmein.collections.hosts.create('1');
	Sshmein.collections.hosts.create('2');
	Sshmein.collections.hosts.create('3');
	Sshmein.collections.hosts.create('3');

	setTimeout(function(){
		var count = Sshmein.collections.hosts.getAllAsArray().length;
		equal(count, 4, 'Expected 4 hosts in collections');
		start();
	}, 600);
});


test('Create 2 hosts, remove 1, remains 1 host', 2, function(){
	stop();
	Sshmein.collections.hosts.create('1');
	Sshmein.collections.hosts.create('2');

	setTimeout(function(){
		var beforeCount = Sshmein.collections.hosts.getAllAsArray().length;

		//Checking count before...
		equal(beforeCount, 2, 'Before :: expected 2 hosts in collections');

		//Removing 1 host
		var hostId = Sshmein.collections.hosts.getAllAsArray()[0].id;
		Sshmein.collections.hosts.remove(hostId);
		var afterCount = Sshmein.collections.hosts.getAllAsArray().length;
		equal(afterCount, 1, 'After :: expected 1 hosts in collections');

		start();
	}, 100);

});

test('Update host name', 2, function(){
	stop();
	Sshmein.collections.hosts.create('1');

	setTimeout(function(){
		var hostId = Sshmein.collections.hosts.getAllAsArray()[0].id;
		var host = Sshmein.collections.hosts.getOne(hostId);
		var currentName = "host_" + hostId;

		//Check current name
		equal(host.name, currentName, "Before :: expected the host's name to be \"" + currentName + "\"");

		//Update host name
		Sshmein.collections.hosts.update(hostId, 'name', 'The new name');
		equal(host.name, 'The new name', "After :: expected the host's name to be \"The new name\"");

		start();
	}, 100);
});


test('Get group ID', function(){
	stop();
	Sshmein.collections.hosts.create('12');

	setTimeout(function(){
		var hostId = Sshmein.collections.hosts.getAllAsArray()[0].id;
		var	groupId = Sshmein.collections.hosts.getGroupId(hostId);
		equal(groupId, 12, "Expected host's group ID to be 12");
		
		start();
	}, 100);
});

test('Delete multiple hosts by group ID', function(){
	stop();
	Sshmein.collections.hosts.create('4');
	Sshmein.collections.hosts.create('7');
	Sshmein.collections.hosts.create('7');
	Sshmein.collections.hosts.create('7');

	setTimeout(function(){
		//Checking count before
		var beforeCount = Sshmein.collections.hosts.getAllAsArray().length;
		equal(beforeCount, 4, 'Before :: expected 4 host in collection');
		
		//Removing groupID 7
		Sshmein.collections.hosts.removeAllByGroupId(7);
		var afterCount = Sshmein.collections.hosts.getAllAsArray().length;
		equal(afterCount, 1, 'After :: expected 1 host in collection');

		start();
	}, 500);
});

/*
Settings API tests
*/

module("Settings' API");

test('Check default display Mode', function(){
	equal(Sshmein.settings.get('defaultDisplayMode'), 'groups', 'Default display mode is groups');
});

test('Check settings setter function', function(){
	equal(Sshmein.settings.get('defaultDisplayMode'), 'groups', 'Before :: expected display mode "groups"');
	Sshmein.settings.set('defaultDisplayMode', "hosts");
	equal(Sshmein.settings.get('defaultDisplayMode'), 'hosts', 'After :: expected display mode "hosts"');
});

test('Check default theme', function(){
	equal(Sshmein.settings.get('theme'), 'Dot Luv', 'Expected "Dot Luv"');
});

test('Check default delay notification', function(){
	equal(Sshmein.settings.get('notificationDelay'), 2000, 'Expected 2000');
});

test('Check hosts view default  filter ', function(){
	equal(Sshmein.settings.get('hostsDefaultFilter'), 'viewAll', 'Expected viewAll');
});

