#!env/bin/python
# -*- coding: utf-8 -*-

from app.models import Group, Host
Group.create_table(fail_silently=True)
Host.create_table(fail_silently=True)
