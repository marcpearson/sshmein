#!/usr/bin/env bash

######################################
#
# SSH_BINARY
#
# Path to ssh executable
#
# Example
# SSH_BINARY=/usr/local/bin/ssh
#
######################################

SSH_BINARY=$(which ssh)

KEY="$HOME/.ssh/id_dsa.pub"

# Checking first if key exists
if [ ! -f $KEY ]; then
    echo "Private DSA key not found at $KEY"
    echo "Please create it first with “ssh-keygen -t dsa"
    echo "Don’t give the key you create with ssh-keygen a password!"
    exit
fi

# Checking if first arg was given on command line
if [ -z $1 ];then
    echo "Please specify user@host (or user@ip address) as the first arg to this script"
    exit
fi

# We can procede ...
echo "Copying your key on $1… "

# Loading key data
KEYCODE=$(cat $KEY)

# Copying key to remote server
$SSH_BINARY -q $1 "mkdir .ssh 2>/dev/null; chmod 700 .ssh; echo $KEYCODE >> .ssh/authorized_keys2; chmod 644 .ssh/authorized_keys2"

echo "Done!"
echo "Please verify your passwordless login now!"
