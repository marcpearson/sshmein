#############################################################

Guide for configuring the broker on a Unix station
to control the reverse SSH tunnel

#############################################################


1. Extract the previously downloaded archive (It's already done!)

2. Copy the extracted folder to the path of your choice

3. Look at the configuration file "sshmein.conf" and make 
   changes if necessary

4. Check files permissions for both "sshmein.sh" and
   "setup_passwordless_login.sh". They must be executable

5. Run (if necessary) "setup_passwordles_login.sh"
   for enabling passwordless SSH connection to the gateway
   you'll use for the reverse SSH tunnel
   
6. Check if the passwordless connection set previously is working

7. Set a schedule task (cron) for checking if a reverse SSH tunnel 
   is requested (15 minutes interval is good)
   
   Example:
   */15 * * * * /path/to/sshmein.sh /path/to/sshmein.conf
   
8. Enjoy!

