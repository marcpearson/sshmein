#############################################################

Guide pour la configuration du broker sur un poste Unix
pour le contrôle du tunnel inversé

#############################################################


1. Extraire l'archive préalablement téléchargée (C'est déjà fait!!!)

2. Copier le répertoire extrait à l'endroit désiré sur le disque

3. Vérifier le fichier de configuration "sshmein.conf" et ajuster les
   paramètres si nécessaire

4. Vérifier les permissions des fichiers "sshmein.sh" et 
   "setup_passwordless_login.sh". Les deux doivent être exécutables

5. Exécuter (si nécessaire) le fichier "setup_passwordles_login.sh"
   pour permettre l"ouverture d'une connexion SSH sans mot de passe
   vers la passerelle que vous désirez utiliser pour le tunnel inversé
   
6. Vérifier si la connexion sans mot de passe vers la passerelle
   préalablement configurée fonctionne correctement

7. Configurer une tâche automatisée (cron) qui vérifiera périodiquement
   (intervalle de 15 minutes est acceptable) si un tunnel inversé doit
   être ouvert
   
   Exemple:
   */15 * * * * /path/to/sshmein.sh /path/to/sshmein.conf
   
8. Amusez-vous!
