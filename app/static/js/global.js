$(document).ready(function() {

  // UPDATED -> NOW WORKS WITH jQuery 1.3.1
  $.fn.listHandlers = function(events, outputFunction) {
      return this.each(function(i){
          var elem = this,
              dEvents = $(this).data('events');
          if (!dEvents) {return;}
          $.each(dEvents, function(name, handler){
              if((new RegExp('^(' + (events === '*' ? '.+' : events.replace(',','|').replace(/^on/i,'')) + ')$' ,'i')).test(name)) {
                 $.each(handler, function(i,handler){
                     outputFunction(elem, '\n' + i + ': [' + name + '] : ' + handler );
                 });
             }
          });
      });
  };

  $(document).ajaxError(function(){
      if (window.console && window.console.error) {
          console.error(arguments);
      }
  });


  String.prototype.trim = function () {
      return this.replace(/^\s*/, "").replace(/\s*$/, "");
  };

  String.prototype.htmlEntities = function () {
     return this.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
  };

  Sshmein.run();

});
