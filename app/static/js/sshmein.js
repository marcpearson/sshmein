/*
* SSHMeIn's javascript code for RIA interface events
* This library make intensive use of jQuery and Amplify libraries
* http://www.jquery.com/
* http://amplifyjs.com/
*/

(function ($, amplify, undefined) {

    /*Export Sshmein as global */
    var Sshmein = this.Sshmein = {};

    /* Detect application URL and PORT */
    Sshmein.application_url = this.location.protocol + '//' + this.location.host + '/';

    /*Stores all hosts and groups API*/
    Sshmein.collections = {};

    /*Store all Sshmein UI management*/
    Sshmein.ui = {};

    /*Store all Sshmein settings*/
    Sshmein.settings = (function(){

        var config = {
            defaultDisplayMode : 'groups',
            theme : 'Dot Luv',
            notificationDelay: 2000, //time in milliseconds
            notificationOpacity : 0.8,
            hostsDefaultFilter : 'viewAll'
        };

        var set = function(key, value){
            config[key] = value;
        };

        var get = function(key){
            return config[key];
        };

        return {
            set : set,
            get: get
        };

    }());//Sshmein.settings

    /*Groups collections API*/
    Sshmein.collections.groups = (function () {

        var store = {};
        var tunnelOnCounters = {};

        var init = function () {

            /*
            * All group's request definition
            */

            /* Fetching all groups */
            amplify.request.define('groups', 'ajax', {
                url: Sshmein.application_url + "groups/",
                dataType: 'json',
                type: 'GET'
            });

            /* Create a new group */
            amplify.request.define('group.create', 'ajax', {
                url: Sshmein.application_url + 'groups/new',
                dataType: 'json',
                type: 'POST'

            });

            /* Delete a group */
            amplify.request.define('group.delete', 'ajax', {
                url: Sshmein.application_url + 'groups/delete/{id}',
                type: 'GET'
            });

            /* Update a group */
            amplify.request.define('group.update', 'ajax', {
                url: Sshmein.application_url + 'groups/update',
                type: 'POST'
            });
        };//init()

        var loadAll = function () {
            tunnelOnCounters = {};
            store = {};

            amplify.request('groups', function (data) {
                for (var i=0; i<data.length; i++) {
                        Sshmein.collections.groups.add(data[i].id, data[i]);
                }
                amplify.publish('groups.load');
            });
        };//loadAll()

        var add = function (groupId, obj) {
            store[groupId] = obj;
        };//add()

        var create = function () {
            amplify.request('group.create', {name: 'newgroup'}, function (data) {
                add(data.id, data);
                amplify.publish('group.create', data.id);
            });
        };//create()

        var remove = function (groupId) {
            delete store[groupId];

            //Removing all host of the group from the hosts collections
            Sshmein.collections.hosts.removeAllByGroupId(groupId);

            //Delete in database
            amplify.request('group.delete', {id : groupId}, function () {
                amplify.publish('group.remove', groupId);
            });

        };//remove()

        var update = function (groupId, property, value) {
            store[groupId][property] = value;

            var data = {id: groupId};
            data[property] = value;
            amplify.request('group.update', data);
            amplify.publish('group.update', {id: groupId, 'key': property, value: value});
        };//update()

        var getOne = function (groupId) {
            if (store[groupId]) {
                return store[groupId];
            }
        };//getOne()

        var getAll = function (asArray) {
            var obj;

            if (asArray === true) {
                var data = [];

                for (obj in store) {
                    if (store.hasOwnProperty(obj)){
                        data.push(store[obj]);
                    }
                }

                return data;

            } else {
                return store;
            }
        };//getAll()


        var incrementTunnelOnCounters = function (groupId) {
            if (!isNaN(tunnelOnCounters[groupId])) {
                tunnelOnCounters[groupId] += 1;
            } else {
                tunnelOnCounters[groupId] = 1;
            }

            amplify.publish('group.counterOn.update', groupId);
        };//incrementTunnelOnCounter()

        var decrementTunnelOnCounters = function (groupId) {
            if (!isNaN(tunnelOnCounters[groupId]) && tunnelOnCounters[groupId] >= 2) {
                tunnelOnCounters[groupId] -= 1;
            } else {
                delete tunnelOnCounters[groupId];
            }

            amplify.publish('group.counterOn.update', groupId);
        };//incrementTunnelOnCounter()

        var getTunnelOnCounters = function () {
            return tunnelOnCounters;
        };//getTunnelOnCounters()

        return {
            /* Groups public methods */

            init : init,
            loadAll : loadAll,
            create : create,
            add : add,
            remove : remove,
            update : update,
            getOne : getOne,
            getAll : getAll,
            incrementTunnelOnCounters : incrementTunnelOnCounters,
            getTunnelOnCounters : getTunnelOnCounters,
            decrementTunnelOnCounters : decrementTunnelOnCounters,

            getAllAsArray : function () {
                return getAll(true);
            }//getAllAsArray()

        };//return object
    }());//Sshmein.collections.groups

    /*Hosts collections API*/
    Sshmein.collections.hosts = (function () {

        var store = {};

        var init = function () {
            /*
            * All host's request definition
            */

            /* Fetching all hosts */
            amplify.request.define('hosts', 'ajax', {
                url: Sshmein.application_url + "hosts/",
                dataType: 'json',
                type: "GET"
            });

            /* Add new host */
            amplify.request.define('host.create', 'ajax', {
                url: Sshmein.application_url + 'hosts/new/{id}',
                dataType: 'json',
                type: 'GET'

            });

            /* Delete a host */
            amplify.request.define('host.delete', 'ajax', {
                url: Sshmein.application_url + 'hosts/delete/{id}',
                type: 'GET'
            });

            /* Update a host */
            amplify.request.define('host.update', 'ajax', {
                url: Sshmein.application_url + 'hosts/update',
                type: 'POST'
            });
        };//init()

        var setAttributes = function (obj) {

            var img_title, img_src, style_class;

            if (obj.tunnel_status.toUpperCase() === 'ON') {
                img_title = Sshmein.language.tunnel_is_on;
                img_src = 'tunnel_on.png';
                style_class = 'tunnel_on';
            } else {
                img_title = Sshmein.language.tunnel_is_off;
                img_src = 'tunnel_off.png';
                style_class = 'tunnel_off';
            }

            obj.style_class = style_class;
            obj.img_title = img_title;
            obj.img_src = img_src;
            obj.lastcheck = Sshmein.time.format(obj.lastcheck);

            return obj;
        };//setAttributes()

        var loadAll = function () {
            amplify.request('hosts', function (data) {
                for (var i=0; i<data.length; i++){
                    var host = setAttributes(data[i]);

                    Sshmein.collections.hosts.add(host.id, host);

                    if (host.tunnel_status.toLowerCase() === 'on') {
                        Sshmein.collections.groups.incrementTunnelOnCounters(host.group_id);
                    }
                }

                amplify.publish('hosts.load');
            });
        };//loadAll()

        var add = function (hostId, obj) {
            store[hostId] = obj;
        };//add()

        var create = function (groupId) {
            amplify.request('host.create', {id: groupId}, function (data) {
                var host = setAttributes(data);

                add(host.id, host);
                amplify.publish('host.create', host.id);
            });
        };//create()

        var remove = function (hostId, fromDatabaseToo) {
            if (store[hostId].tunnel_status.toLowerCase() === 'on'){
                Sshmein.collections.groups.decrementTunnelOnCounters(store[hostId].group_id);
            }

            delete store[hostId];

            amplify.publish('host.remove', hostId);

            if (fromDatabaseToo !== false) {
                //Delete in database
                amplify.request('host.delete', {id : hostId});
            }
        };//remove()

        var update = function (hostId, property, value) {
            store[hostId][property] = value;

            var data = {id: hostId};
            data[property] = value;
            amplify.request('host.update', data);
            amplify.publish('host.update', {id: hostId, 'key': property, value: value});
        };//update()

        var getOne = function (hostId) {
            if (store[hostId]) {
                return store[hostId];
            }
        };//getOne()

        var getAll = function (asArray) {
            var obj;

            if (asArray === true) {
                var data = [];

                for (obj in store) {
                    if (store.hasOwnProperty(obj)){
                        data.push(store[obj]);
                    }
                }

                return data;

            } else {
                return store;
            }
        };//getAll()

        var removeAllByGroupId = function (groupId) {
            var hostId;

            for (hostId in store) {
                if (store.hasOwnProperty(hostId) && store[hostId].group_id == groupId) {
                    remove(hostId, false);
                }
            }
        };//removeAllByGroupId()

        var getGroupId = function (hostId) {
            return store[hostId].group_id;
        };//getGroupId()


        return {
            /* Hosts public methods */

            init : init,
            loadAll : loadAll,
            create : create,
            add : add,
            remove : remove,
            update : update,
            getOne : getOne,
            getAll : getAll,
            removeAllByGroupId : removeAllByGroupId,
            getGroupId : getGroupId,

            getAllAsArray : function () {
                return getAll(true);
            }//getAllAsArray()

        };//return object
    }());//Sshmein.collections.hosts


    /*
    * Models definition initialization
    */
    Sshmein.models = {
        init: function () {
            Sshmein.collections.groups.init();
            Sshmein.collections.hosts.init();
        }//init()
    };//Sshmein.models


    /*
    * Sshmein UI Groups
    */

    Sshmein.ui.groups = {

        load : function () {
            Sshmein.collections.groups.loadAll();
        },//load()

        add : function () {
            Sshmein.collections.groups.create();
        },//add()

        remove : function (groupId) {

            var groupEl = $('div#group_' + groupId);
            var title = groupEl.attr('data-groupname');

            //Requesting confirmation before deleting
            var resp = confirm(Sshmein.language.delete_group_confirm +  ' "' + title + '" ' + Sshmein.language.and_all_his_hosts + '?\n' + Sshmein.language.after_the_deletion);

            if (resp) {
                //Deleting group from collections
                Sshmein.collections.groups.remove(groupId);

                //Removing group from interface
                $('div#group_' + groupId).remove();

                //Notification
                amplify.publish('notification', Sshmein.language.deletion_of_group + ' ' + title + ' OK!');
            }
        },//remove()

        renderAll : function () {

            var data = Sshmein.collections.groups.getAllAsArray();

            $('#groups').html('');

            $('#groupTemplate').tmpl(data).appendTo('#groups');

            $("#groups").multiAccordion('destroy');
            $("#groups").multiAccordion({active: 'none' });

            //Binding edit in place
            Sshmein.ui.groups.lateBinding();

        },//render()


        renderOne: function (groupId) {
            var data = Sshmein.collections.groups.getOne(groupId);

            $('#groupTemplate').tmpl(data).prependTo('#groups');

            var activePanels = $('#groups').multiAccordion('getActiveTabs');

            $("#groups").multiAccordion('destroy');
            $("#groups").multiAccordion({active : activePanels});

            //Binding edit in place
            Sshmein.ui.groups.lateBinding();

        },//renderOne()

        updateGroupName : function (groupId, newName) {
            $('div#group_' + groupId).attr('data-groupname', newName);
            $('#delgroup_' + groupId).attr('title', Sshmein.language.delete_group + ' ' + newName);
            $('td[data-group=' + groupId + ']', '#hosts').html(newName);
        },//updateGroupName()


        renderCounterOn: function (groupId) {
            var counters = Sshmein.collections.groups.getTunnelOnCounters();
            var tunnelOnCnt = counters[groupId];
            var text = (tunnelOnCnt ? 'Tunnel ON : ' + tunnelOnCnt : '');

            $('#tunnelON_' + groupId).html(text);

        },//renderCounterOn

        expandAll: function(){
            var closedGroups = $('#groups').find('div h3').filter(
                function(){
                    return $(this).hasClass('ui-state-default');
                }
            );

            closedGroups.each(function(){
                $(this).trigger('click');
            });
        },//expandAll()

        collapseAll:  function(){
            var openedGroups = $('#groups').find('div h3').filter(
                function(){
                    return $(this).hasClass('ui-state-active');
                }
            );

            openedGroups.each(function(){
                $(this).trigger('click');
            });
        },//collapseAll()

        refresh : function(){
            $('#groups-toolbar').hide();
            this.load();
        },

        lateBinding: function () {
            //Binding edit in place for group's name
            $('.editgroupname').editable(function (value) {
                Sshmein.collections.groups.update(this.id, 'name', value);
                return value;

            }, {
                name : 'groupname',
                onblur : 'submit',
                style : 'width: 200px;',
                tooltip : Sshmein.language.edit_group_name,
                width: 'none',
                event: 'dblclick'
            });

            //Binding edit in place for group's description
            $('.editgroupdescription').editable(function (value) {
                Sshmein.collections.groups.update(this.id, 'description', value);
                return value;
            }, {
                name : 'description',
                type : 'textarea',
                rows : 3,
                submit: 'OK',
                tooltip : Sshmein.language.edit_group_description,
                placeholder : Sshmein.language.click_to_edit
            });
        },

        events : function () {
            //Binding event for delete group image
            $('#groups').on( 'click', 'a.delgroup', function () {
                Sshmein.ui.groups.remove($(this).attr('id').substring(9));
            });

            //Binding event for New host button
            $('#groups').on('click', 'button.new_host', function () {
                Sshmein.collections.hosts.create($(this).attr('id').substring(13));
            });

            /*
            * Binding event for view buttons
            */
            //View ALL
            $('#groups').on('click', 'button.view_all', function () {
                $('dl.host').show();
                $(this).parents('.filter:first').find(".ui-state-active").removeClass("ui-state-active");
                $(this).addClass('ui-state-active');
            });

            //View ON
            $('#groups').on('click', 'button.view_on', function () {
                $('dl.tunnel_on').show();
                $('dl.tunnel_off').hide();
                $(this).parents('.filter:first').find(".ui-state-active").removeClass("ui-state-active");
                $(this).addClass('ui-state-active');
            });

            //View OFF
            $('#groups').on('click', 'button.view_off', function () {
                $('dl.tunnel_off').show();
                $('dl.tunnel_on').hide();
                $(this).parents('.filter:first').find(".ui-state-active").removeClass("ui-state-active");
                $(this).addClass('ui-state-active');
            });

            $('#groups').on('hover', 'dl', function(event){
                if (event.type === 'mouseenter'){
                    $(this).addClass('ui-state-active ui-corner-all');
                }else{
                    $(this).removeClass('ui-state-active ui-corner-all');
                }
            });

            $('#groups').on('hover', 'button', function (event){
                if (event.type === 'mouseenter'){
                    $(this).addClass('ui-state-hover');
                }else{
                    $(this).removeClass('ui-state-hover');
                }
            });

            //Binding click expand All
            $('#expandAll').on('click', function () {
                Sshmein.ui.groups.expandAll();
            });

            //Binding click collapse All
            $('#collapseAll').on('click', function () {
                Sshmein.ui.groups.collapseAll();
            });
        }//events
    };//Sshmein.ui.groups

    /*
    * Sshmein UI hosts
    */

    Sshmein.ui.hosts = {
        load : function () {
            Sshmein.collections.hosts.loadAll();
        },//load()

        add : function () {
            Sshmein.collections.hosts.create();
        },//add()

        destroy : function (hostId) {

            var title = Sshmein.collections.hosts.getOne(hostId).hostname;

            //Requesting confirmation before deleting
            var resp = confirm(Sshmein.language.delete_host_confirm + ' "' + title + '" ?');

            if (resp) {
                //Removing from collections
                Sshmein.collections.hosts.remove(hostId);

                //Notification
                amplify.publish('notification', Sshmein.language.deletion_of_host + ' ' + title + ' OK!');
            }
        },//destroy()

        remove : function(hostId){
            $('[data-host=' + hostId + ']').remove();
        },//remove()

        renderAll : function () {
            var row;

            var data = Sshmein.collections.hosts.getAll();

            var hostsContainer = $('#hosts');

            /* Clear hosts view content */
            hostsContainer.html('');

            $('#hostsTable').tmpl().appendTo(hostsContainer);

            for (row in data) {
                Sshmein.ui.sidebar.updateList(data[row].id, data[row].tunnel_status);

                var groupId = '#group_' + data[row].group_id;
                data[row].groupname = Sshmein.collections.groups.getOne(data[row].group_id).name;

                if (data[row].log){
                    var regex = new RegExp('([0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2} Z)');
                    var matches = data[row].log.match(regex);
                    if (matches){
                        var z_datetime = matches[0];
                        local_formatted_datetime = Sshmein.time.format(z_datetime);
                        data[row].log = data[row].log.replace(regex, local_formatted_datetime);
                    }
                }

                /* Add host in tile view*/
                if ($("dl[data-host='" + data[row].id + "']").length === 0){
                    $('#hostTemplate').tmpl(data[row]).appendTo(groupId + ' div.hosts');
                }

                /* Add host in detail view*/
                $('#hostTemplateRow').tmpl(data[row]).appendTo('tbody', hostsContainer);
            }

            //Binding edit in place
            Sshmein.ui.hosts.lateBinding();

        },//renderAll

        renderOne: function (hostId) {
            var data = Sshmein.collections.hosts.getOne(hostId);
            data.groupname = Sshmein.collections.groups.getOne(data.group_id).name;

            /* Add host in tile view*/
            $('#hostTemplate').tmpl(data).insertAfter('#group_' + data.group_id + ' div.hosts div.ui-widget-header');

            /* Add host in detail view*/
            $('#hostTemplateRow').tmpl(data).appendTo('#hosts tbody');

            //Binding edit in place
            Sshmein.ui.hosts.lateBinding();

        },//renderOne()

        tunnelStatus: function (hostId, value) {
            var state = (value === 'ON' ? 'on' : 'off');
            var hostname = Sshmein.collections.hosts.getOne(hostId).hostname;
            var notificationType = ( state == 'off' ? 'error' : 'success' );
            var icon = $('.tunnel', '[data-host=' + hostId + ']');

            if (value === 'ON') {
                img_title = Sshmein.language.tunnel_is_on;
                notification_msg = Sshmein.language.turning_on;
            }else{
                img_title = Sshmein.language.tunnel_is_off;
                notification_msg = Sshmein.language.turning_off;
            }

            $('[data-host=' + hostId +']').attr('class', 'host tunnel_' + state);
            icon.attr('src', '/static/images/tunnel_' + state + '.png');
            icon.attr('class', 'tunnel tunnel_' + state);
            icon.attr('title', img_title);

            amplify.publish('notification', notification_msg, hostname, notificationType );
        },

        updateHostName: function (hostId, newName) {
            $('img.delete','[data-host=' + hostId + ']').attr('title', Sshmein.language.delete_host + ' ' + newName);
            $('a.download img','[data-host=' + hostId + ']').attr('title', Sshmein.language.download_tunnel_client + ' ' + newName);
            $('[data-host=' + hostId + ']').find('.edit_hostname').html(newName);
        },//updateHostName()

        updateGatewayHost: function(hostId, gateway){
            $('[data-host=' + hostId + ']').find('.edit_gateway_host').html(gateway);
        },

        updateGatewayPort: function(hostId, port){
            $('[data-host=' + hostId + ']').find('.edit_gateway_port').html(port);
        },

        updateGatewayUser: function(hostId, user){
            $('[data-host=' + hostId + ']').find('.edit_gateway_user').html(user);
        },

        viewAll : function(){
            $('.filter', '#hosts-toolbar').find(".ui-state-active").removeClass("ui-state-active");
            $('.view_all', '#hosts-toolbar').addClass('ui-state-active');
            $('tr', '#hosts').each(function(){
                $(this).show();
            });
        },

        viewOn : function(){
            this.viewAll();
            $('.filter', '#hosts-toolbar').find(".ui-state-active").removeClass("ui-state-active");
            $('.view_on', '#hosts-toolbar').addClass('ui-state-active');
            $('img.tunnel_off', '#hosts').each(function(){
                $(this).parents('tr').hide();
            });
        },

        viewOff : function(){
            this.viewAll();
            $('.filter', '#hosts-toolbar').find(".ui-state-active").removeClass("ui-state-active");
            $('.view_off', '#hosts-toolbar').addClass('ui-state-active');
            $('img.tunnel_on', '#hosts').each(function(){
                $(this).parents('tr').hide();
            });
        },

        refresh : function(){
            $('#hosts-toolbar').hide();
            Sshmein.ui.groups.refresh();
        },

        postRefresh: function(){
            var activeFilter = Sshmein.settings.get('hostsActiveFilter') || Sshmein.settings.get('hostsDefaultFilter');
            Sshmein.ui.hosts[activeFilter]();
        },

        lateBinding : function () {
            $('.edit_hostname').editable(function (value) {
                var hostId = $(this).parents('[data-host]').attr('data-host');
                Sshmein.collections.hosts.update(hostId, 'hostname', value);
                return value;
            }, {
                name : 'hostname',
                onblur : 'submit',
                tooltip : Sshmein.language.edit_hostname,
                style: "display:inline",
                placeholder : Sshmein.language.click_to_edit,
                cssclass: "hostname",
                width: "none"
            });

            $('.edit_gateway_host').editable(function (value) {
                var hostId = $(this).parents('[data-host]').attr('data-host');
                Sshmein.collections.hosts.update(hostId, 'gateway_host', value);
                return value;
            }, {
                name : 'gateway_host',
                onblur : 'submit',
                tooltip : Sshmein.language.edit_gateway,
                placeholder : Sshmein.language.click_to_edit,
                style: "display:inline"
            });

            $('.edit_gateway_port').editable(function (value) {
                var hostId = $(this).parents('[data-host]').attr('data-host');
                Sshmein.collections.hosts.update(hostId, 'gateway_port', value);
                return value;
            }, {
                name : 'gateway_port',
                onblur : 'submit',
                tooltip : Sshmein.language.edit_port,
                style: "display:inline",
                placeholder : Sshmein.language.click_to_edit,
                width:"50px"
            });

            $('.edit_gateway_user').editable(function (value) {
                var hostId = $(this).parents('[data-host]').attr('data-host');
                Sshmein.collections.hosts.update(hostId, 'gateway_user', value);
                return value;
            }, {
                name : 'gateway_user',
                onblur : 'submit',
                tooltip : Sshmein.language.edit_user,
                style: "display:inline",
                placeholder : Sshmein.language.click_to_edit,
                width:"30px"
            });

            $('.edit_authorized_ip').editable(function (value) {
                var hostId = $(this).parents('[data-host]').attr('data-host');
                Sshmein.collections.hosts.update(hostId, 'authorized_ip', value);
                return value;
            }, {
                name : 'authorized_ip',
                onblur : 'submit',
                tooltip : Sshmein.language.edit_authorized_ip,
                style: "display:inline",
                placeholder : Sshmein.language.click_to_edit

            });
        },//lateBinding()

        events : function () {

            $('#groups, #hosts').on('click', 'img.tunnel_on', function () {
                var hostId = $(this).parents('[data-host]').attr('data-host');
                Sshmein.collections.hosts.update(hostId, 'tunnel_status', 'OFF');
            });

            $('#groups, #hosts').on('click', 'img.tunnel_off', function () {
                var hostId = $(this).parents('[data-host]').attr('data-host');
                Sshmein.collections.hosts.update(hostId, 'tunnel_status', 'ON');
            });

            $('#groups, #hosts').on('click', 'img.delete', function () {
                var hostId = $(this).parents('[data-host]').attr('data-host');
                Sshmein.ui.hosts.destroy(hostId);
            });

            $('#hosts').on('click', '[data-group]', function () {
                var groupId = $(this).attr('data-group');
                Sshmein.ui.actions.openGroup(groupId);
            });

            $('#hosts').on('hover', 'tbody tr', function(event){
                if (event.type === 'mouseenter'){
                    $(this).addClass('ui-state-active');
                }else{
                    $(this).removeClass('ui-state-active');
                }
            });

            $('.view_all', '#hosts-toolbar').on('click', function(){
                Sshmein.settings.set('hostsActiveFilter', 'viewAll');
                Sshmein.ui.hosts.viewAll();
            });

            $('.view_on', '#hosts-toolbar').on('click', function(){
                Sshmein.settings.set('hostsActiveFilter', 'viewOn');
                Sshmein.ui.hosts.viewOn();
            });

            $('.view_off', '#hosts-toolbar').on('click', function(){
                Sshmein.settings.set('hostsActiveFilter', 'viewOff');
                Sshmein.ui.hosts.viewOff();
            });

        }//events()
    };//Sshmein.ui.hosts

    /*
    * Sshmein UI sidebar
    */

    Sshmein.ui.sidebar = (function () {

        var tunnelOnCounter = 0;

        var renderCounter = function () {
            $('p#tunnel_on_count span').html(tunnelOnCounter);
        };//_renderCounter()

        var incrementCounter = function () {
            tunnelOnCounter++;
            renderCounter();
        };//incrementCounter()

        var decrementCounter = function () {
            tunnelOnCounter--;
            tunnelOnCounter = tunnelOnCounter < 0 ? 0 : tunnelOnCounter;
            renderCounter();
        };//decrementCounter()

        var updateGroupName = function (groupId, groupName) {
            var el = $('div#aside_info li[data-group=' + groupId + ']');

            if (el) {
                el.attr('title', groupName);
            }
        };//updateGroupName()

        var updateHostName = function (hostId, newName) {

            var  elem = $('div#aside_info li[data-host=' + hostId + '] a');

            if (elem) {
                elem.html(newName.htmlEntities());
            }
        };//updateHostName()

        var removeHost = function (hostId) {

            var elem = $('div#aside_info li[data-host=' + hostId + ']');

            if (elem.length === 1) {
                elem.remove();
                decrementCounter();
            }

        };//removeHost();

        var addHost = function (hostId) {

            var host = Sshmein.collections.hosts.getOne(hostId);
            var groupname = Sshmein.collections.groups.getOne(host.group_id).name;

            if ($('aside li[data-host=' + hostId + ']').length === 0) {
                $('aside ul').append('<li data-host="' + hostId + '" data-group="' + host.group_id + '" title="' + groupname + '"><a href="#">' + host.hostname + '</a></li>');
                incrementCounter();
            }
        };//addHost();

        var updateList = function (hostId, tunnelStatus) {
            if (tunnelStatus.toLowerCase() === 'on') {
                addHost(hostId);
            } else {
                removeHost(hostId);
            }
        };//updateList()

        return {

            updateList : updateList,
            updateGroupName : updateGroupName,
            updateHostName : updateHostName

        };//return
    }());//Sshmein.ui.sidebar

    Sshmein.ui.actions =(function () {

        var toggleView = function(view){
            Sshmein.settings.set('displayMode', view);
            amplify.publish('toggle.view', view);
        };

        var refresh = function(){
            amplify.publish('notification', Sshmein.language.refreshing + '...', '', 'info');
            amplify.publish('refresh');
        };

        var translate = function(locale){
            var redirect = Sshmein.application_url + locale;
            window.location.replace(redirect);
        };

        var openGroup = function (groupId) {
            Sshmein.ui.actions.toggleView('groups');
            var groupEl = '#group_' + groupId;
            var h3 = $('h3', groupEl);
            var isClosed = h3.hasClass('ui-state-default');

            if (isClosed){
                h3.trigger('click');
            }

            $('html,body').animate({
                scrollTop: '+=' + $(groupEl).offset().top + 'px'
            }, 'slow');

        };//openGroup()

        var searchHost = function(){
            var elem = $('input[name=searchHost]');
            var hostname = elem.val().toLowerCase();

            var regex = RegExp(hostname);

            if (hostname){
                var elements = $('.edit_hostname', '#groups').filter(
                    function(){
                        return $(this).html().toLowerCase().match(regex);
                    }
                );

                if (elements.length > 0){

                    Sshmein.ui.groups.collapseAll();

                    elements.each(function(){
                        var hostId = $(this).parents('dl').attr('data-host');
                        var groupId = Sshmein.collections.hosts.getOne(hostId).group_id;
                        openGroup(groupId);
                    });

                }else{
                    //No host match
                    amplify.publish('notification', Sshmein.language.no_host_match);
                }

                elem.val('');

            }
        };//searchHost()

        return {

            toggleView : toggleView,
            refresh : refresh,
            translate: translate,
            openGroup: openGroup,
            searchHost: searchHost
        };//return
    }());//Sshmein.ui.actions

    Sshmein.ui.events = function () {
        //Hover effect for the logout button
        $('#logout, button').hover(function () {
            $(this).addClass('ui-state-hover');
        }, function () {
            $(this).removeClass('ui-state-hover');
        });

        //Binding logout event to logout button
        $('#logout').on('click', function () {
            /*Nothing here now!*/
        });

        //Binding click 'New group' button
        $('button#new_group').on('click', function () {
            Sshmein.ui.groups.add();
        });

        //Binding change 'translate' dropdown
        $('#translate').on('change', function () {
            var selectedLocale = $(this).val();
            Sshmein.ui.actions.translate(selectedLocale);
        });

        //Binding click 'Refresh' button
        $('button#refresh').on('click',function () {
            Sshmein.ui.actions.refresh();
        });

        //Binding sidebar's href to openGroup functionnality
        $('#aside_info').on('click', 'a', function () {
            var groupId = $(this).parent().attr('data-group');
            var hostId = $(this).parent().attr('data-host');
            $('dl[data-host="' + hostId + '"]').trigger('mouseover');
            Sshmein.ui.actions.openGroup(groupId);
        });

        //Binding click View by Groups button
        $('button#displayByGroups').on('click', function () {
            Sshmein.ui.actions.toggleView('groups');
        });

        //Binding click View by Hosts button
        $('button#displayByHosts').on('click', function () {
            Sshmein.ui.actions.toggleView('hosts');
        });

        //Binding Keypress (enter) on search host input
        $('input[name=searchHost]').keyup(function(evt){
            if (evt.which === 13){
                Sshmein.ui.actions.searchHost();
            }
        });

        //All group's events
        Sshmein.ui.groups.events();

        //All host's events
        Sshmein.ui.hosts.events();

    };//Sshmein.ui.events

    Sshmein.subscriptions = function () {
        amplify.subscribe('groups.load', function () {
            if (Sshmein.collections.groups.getAll()) {
                Sshmein.ui.groups.renderAll();
                $('#groups-toolbar').show();
                Sshmein.ui.hosts.load();
            } else {
                $('#loadingDiv').dialog('destroy');
            }
        });

        amplify.subscribe('group.create', function (groupId) {
            Sshmein.ui.groups.renderOne(groupId);
        });

        amplify.subscribe('group.update', function (obj) {
            var groupId = obj.id;
            var property = obj.key;
            var value = obj.value;

            switch (property){
                case 'name':
                    Sshmein.ui.sidebar.updateGroupName(groupId, value);
                    Sshmein.ui.groups.updateGroupName(groupId, value);
                break;
            }
        });

        amplify.subscribe('hosts.load', function () {
            Sshmein.ui.hosts.renderAll();
            $('#hosts-toolbar').show();
            $('#refresh').removeAttr('disabled');
            $('#loadingDiv').dialog('destroy');
            Sshmein.ui.hosts.postRefresh();
        });

        amplify.subscribe('host.create', function (hostId) {
            Sshmein.ui.hosts.renderOne(hostId);
        });

        amplify.subscribe('notification', function (msg, title, type) {
            Sshmein.notifiate(msg, title, type);
        });

        amplify.subscribe('host.remove', function (hostId) {
            Sshmein.ui.sidebar.updateList(hostId, 'off');
            Sshmein.ui.hosts.remove(hostId);
        });

        amplify.subscribe('host.update', function (obj) {
            var hostId = obj.id;
            var property = obj.key;
            var value = obj.value;

            switch (property) {
                case 'tunnel_status':
                    Sshmein.ui.hosts.tunnelStatus(hostId, value);
                    Sshmein.ui.sidebar.updateList(hostId, value);

                    var groupId = Sshmein.collections.hosts.getGroupId(hostId);

                    if (value.toLowerCase() === 'on') {
                        Sshmein.collections.groups.incrementTunnelOnCounters(groupId);
                    } else {
                        Sshmein.collections.groups.decrementTunnelOnCounters(groupId);
                    }
                break;

                case 'hostname':
                    Sshmein.ui.sidebar.updateHostName(hostId, value);
                    Sshmein.ui.hosts.updateHostName(hostId, value);
                break;

                case 'gateway_host':
                    Sshmein.ui.hosts.updateGatewayHost(hostId, value);
                break;

                case 'gateway_port':
                    Sshmein.ui.hosts.updateGatewayPort(hostId, value);
                break;

                case 'gateway_user':
                    Sshmein.ui.hosts.updateGatewayUser(hostId, value);
                break;
            }
        });

        amplify.subscribe('group.counterOn.update', function (groupId) {
            Sshmein.ui.groups.renderCounterOn(groupId);
        });


        amplify.subscribe('toggle.view', function(mode){

            if (mode === 'groups'){
                $('#groups-wrapper').show();
                $('#hosts-wrapper').hide();
                $('button#displayByHosts').removeClass("ui-state-active");
                $('button#displayByGroups').addClass('ui-state-active');
            }else{
                $('#groups-wrapper').hide();
                $('#hosts-wrapper').show();
                $('button#displayByHosts').addClass("ui-state-active");
                $('button#displayByGroups').removeClass('ui-state-active');
            }

        });

        amplify.subscribe('refresh', function () {
            $('#refresh').attr('disabled', 'disabled');
            //$('.filter', '#hosts-toolbar').find(".ui-state-active").removeClass("ui-state-active");
            //$('.view_all', '#hosts-toolbar').addClass('ui-state-active');

            var currentdisplayMode = Sshmein.settings.get('displayMode');

            $('#' +  currentdisplayMode).html('<div id="loader">' + Sshmein.language.waiting_for_data + '...</div>');

            if (currentdisplayMode == 'groups'){
                Sshmein.ui.groups.refresh();
            }else{
                Sshmein.ui.hosts.refresh();
            }
        });
    };//Sshmein.subscriptions

    Sshmein.notifiate = function (msg, title, type) {

        type = type || 'success';
        title = title || '';

        switch(type){
            case 'success':
                Notifier.success(msg, title);
            break;

            case 'info':
                Notifier.info(msg, title);
            break;

            case 'warning':
                Notifier.warning(msg, title);
            break;

            case 'error':
                Notifier.error(msg, title);
            break;
        }
    };//notifiate()

    Sshmein.time = {

		format: function(utc_stamp){
            if (utc_stamp){
                return moment(utc_stamp).format('LLL');
			}else{
                return '';
			}
		}//format()
	};//Sshmein.time

    Sshmein.run = function () {
        /*
        * Starting SSHMeIn
        */
        $('#loadingDiv').dialog({modal: true, resizable: false, title: Sshmein.language.loading_sshmein_interface, height: 120, width: 400}).show();
        $('#switcher').themeswitcher({loadTheme: Sshmein.settings.get('theme'), imgpath: '/static/images/themeGallery/'});

        this.subscriptions();
        this.models.init();
        this.ui.events();
        this.ui.groups.load();

        //Set NotifierJs config
        NotifierjsConfig.defaultTimeOut = Sshmein.settings.get('notificationDelay');
        NotifierjsConfig.notificationStyles.opacity = Sshmein.settings.get('notificationOpacity');
        NotifierjsConfig.position = ["bottom", "right"];
        NotifierjsConfig.notificationStyles.width = '320px';

        //Enabling default display
        Sshmein.ui.actions.toggleView(Sshmein.settings.get('defaultDisplayMode'));

    };//Sshmein.run

}(jQuery, amplify));

