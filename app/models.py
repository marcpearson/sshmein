# -*- coding: utf-8 -*-
from app import app, db
from peewee import Model, DoesNotExist, IntegerField, CharField, TextField


class Group(Model):

    class Meta:
        table_name = 'groups'
        database = db

    owner_id = IntegerField()
    name = CharField(max_length=50)
    description = TextField(default='')

    def new(self, name):

        owner_id = 0

        return self.create(owner_id=owner_id, name=name)

    def get_one_by_id(self, id):

        owner_id = 0

        try:
            return self.select().where(Group.id == id, Group.owner_id == owner_id).get()
        except DoesNotExist:
            return None

    def change(self, **kwargs):

        if 'name' in kwargs:
            self.name = kwargs['name']

        if 'description' in kwargs:
            self.description = kwargs['description']

        self.save()

    def remove(self):
        return self.delete_instance()

    def get_all_by_user(self):

        owner_id = 0

        data = self.select().where(Group.owner_id == owner_id).order_by(Group.name)

        if data:
            return data
        else:
            return None

    def __unicode__(self):
        return self.name


class Host(Model):

    class Meta:
        table_name = 'hosts'
        database = db

    owner_id = IntegerField()
    group_id = IntegerField()
    hostname = CharField(max_length=30)
    tunnel_status = CharField(max_length=3)
    tunnel_key = CharField(max_length=36)
    gateway_port = IntegerField(default=0)
    gateway_host = CharField()
    gateway_user = CharField(max_length=30)
    authorized_ip = CharField(max_length=30, default='')
    lastcheck = IntegerField(default=0)
    log = CharField(null=True)

    def new(self, group_id, **kwargs):

        if Group().get_one_by_id(group_id):

            self.owner_id = 0
            self.group_id = group_id
            self.set_properties(**kwargs)
            self.save()

            return self
        else:
            return ''

    def get_one_by_id(self, id):

        owner_id = 0

        try:
            return self.select().where(Host.id == id, Host.owner_id == owner_id).get()
        except DoesNotExist:
            return None

    def change(self, **kwargs):
        self.set_properties(**kwargs)
        self.save()

    def remove(self):
        return self.delete_instance()

    def get_all_by_user(self):

        owner_id = 0

        data = self.select().where(Host.owner_id == owner_id).order_by(Host.hostname)

        if data:
            return data
        else:
            return None

    def get_all_by_group_id(self, group_id):

        owner_id = 0

        data = self.select().where(Host.owner_id == owner_id,
                                   Host.group_id == group_id).order_by(Host.hostname)

        if data:
            return data
        else:
            return None

    def get_one_by_tunnel_key(self, tunnel_key):

        try:
            return self.select().where(Host.tunnel_key == tunnel_key).get()
        except DoesNotExist:
            return None

    def set_properties(self, **kwargs):
        for k, v in kwargs.items():

            if (k == 'gateway_port'):
                try:
                    v = int(v)
                except:
                    v = 0

            setattr(self, k, v)

    def __unicode__(self):
        return self.hostname
