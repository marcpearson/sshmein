# -*- coding: utf-8 -*-

from flask import render_template, request, json, Blueprint, Response, make_response, escape
from flask_babel import gettext
from app import app
from .models import Group, Host
import uuid
from datetime import datetime

groups = Blueprint('groups', __name__)
hosts = Blueprint('hosts', __name__)


@app.route('/')
def default():
    return render_template('index.html', version=app.config['VERSION'])


@app.route('/<lang>/')
@app.route('/<lang>/index')
def index(lang):
    return render_template('index.html', version=app.config['VERSION'])


@groups.route('/', methods=['GET'])
def list():

    data = []
    groups = Group().get_all_by_user()

    if groups:
        data = [{'id': group.id, 'name': escape(group.name), 'description': escape(
            group.description)} for group in groups]

    js = json.dumps(data, separators=(',', ':'))

    return Response(js, 200, mimetype='application/json')


@groups.route('/new', methods=['POST'])
def new():

    group = Group().new(request.form['name'])

    data = {'id': group.id, 'name': escape(
        group.name), 'description': escape(group.description)}

    js = json.dumps(data)

    return Response(js, 200, mimetype='application/json')


@groups.route('/update', methods=['POST'])
def update():

    group = Group().get_one_by_id(request.form['id'])

    if group:
        if 'name' in request.form:
            value = request.form['name']
            group.change(name=value)

        if 'description' in request.form:
            value = request.form['description']
            group.change(description=value)

    return Response(escape(value), 200)


@groups.route('/delete/<int:id>', methods=['GET'])
def delete(id):

    group = Group().get_one_by_id(id=id)

    if group:
        # Deleting group's hosts
        hosts = Host().get_all_by_group_id(id)

        if hosts:
            for host in hosts:
                host.remove()
        count = group.remove()

    return Response(escape(count), 200)


@hosts.route('/', methods=['GET'])
def list():

    data = []
    hosts = Host().get_all_by_user()

    if hosts:
        data = [{
            'id': host.id,
            'group_id': host.group_id,
            'hostname': host.hostname,
            'tunnel_status': host.tunnel_status,
            'tunnel_key': host.tunnel_key,
            'gateway_port': host.gateway_port,
            'gateway_host': host.gateway_host,
            'gateway_user': host.gateway_user,
            'authorized_ip': host.authorized_ip,
            'lastcheck': datetime.fromtimestamp(host.lastcheck).strftime("%Y-%m-%dT%H:%M:%S Z") if host.lastcheck else '',
            'log': host.log
        } for host in hosts]

    js = json.dumps(data, separators=(',', ':'))

    return Response(js, 200, mimetype='application/json')


@hosts.route('/new/<int:group_id>', methods=['GET'])
def new(group_id):

    tunnel_key = uuid.uuid4()
    host = Host().new(group_id, tunnel_key=tunnel_key, tunnel_status='OFF',
                      hostname='host', gateway_host='gateway', gateway_user='user')

    if host:
        data = {
            "id": host.id,
            "group_id": host.group_id,
            "tunnel_key": str(host.tunnel_key),
            "hostname": host.hostname,
            "tunnel_status": host.tunnel_status,
            "gateway_port": host.gateway_port,
            "gateway_host": host.gateway_host,
            "gateway_user": host.gateway_user
        }

        js = json.dumps(data)

        return Response(js, 200, mimetype='application/json')
    else:
        return Response('')


@hosts.route('/update', methods=['POST'])
def update():

    host = Host().get_one_by_id(request.form['id'])

    if host:

        if 'hostname' in request.form:
            value = request.form['hostname']
            host.change(hostname=value)

        elif 'gateway_host' in request.form:
            value = request.form['gateway_host']
            host.change(gateway_host=value)

        elif 'gateway_port' in request.form:
            value = request.form['gateway_port']
            host.change(gateway_port=value)

        elif 'gateway_user' in request.form:
            value = request.form['gateway_user']
            host.change(gateway_user=value)

        elif 'authorized_ip' in request.form:
            value = request.form['authorized_ip']
            host.change(authorized_ip=value)

        elif 'tunnel_status' in request.form:
            value = request.form['tunnel_status']
            host.change(tunnel_status=value)

            # Update host's log field on tunnel_status change
            host.change(log=gettext("Tunnel was turn %(status)s at %(time)s",
                                    status=value, time=datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S Z")))

    return Response(escape(value), 200)


@hosts.route('/delete/<int:id>', methods=['GET'])
def delete(id):

    host = Host().get_one_by_id(id=id)

    if host:
        count = host.remove()

    return Response(escape(count), 200)


@hosts.route('/broker/<int:id>', methods=['GET'])
def broker(id):

    host = Host().get_one_by_id(id=id)

    response = ''

    if host:
        import io
        import tarfile
        import re

        # File object for on-the-fly tar.gz archive
        output = io.BytesIO()
        archive = tarfile.open(name=None, mode='w:gz', fileobj=output)

        # reading base template for configuration file and replace %replaceme% placeholder with tunnel_key
        data = open('broker/sshmein.conf').read()
        generated_conf_file = io.StringIO(
            re.sub("%replaceme%", host.tunnel_key, data))

        # Create tarinfo object name and file size and add on-the-fly generated file to the archive
        tar_info = tarfile.TarInfo('broker/sshmein.conf')
        tar_info.size = len(generated_conf_file.getvalue())
        archive.addfile(tar_info, fileobj=io.BytesIO(
            generated_conf_file.read().encode('utf8')))

        # Adding other necessary files to the archive
        archive.add('broker/sshmein.sh')
        archive.add('broker/README-EN.txt')
        archive.add('broker/README-FR.txt')
        archive.add('broker/setup_passwordless_login.sh')

        # Closing file handle
        generated_conf_file.close()
        archive.close()

        # Getting archive content and closing handle
        contents = output.getvalue()
        output.close()

        # Sending file to the browser
        response = make_response(contents)
        response.headers['Content-Description'] = 'File Transfer'
        response.headers['Content-Type'] = 'application/x-gzip'
        response.headers['Content-Disposition'] = 'attachment; filename=%s_sshmein_broker.tar.gz' % (
            host.hostname,)
        response.headers['Cache-Control'] = 'no-cache'

    return response


@app.route('/services/tunnel/<tunnel_key>', methods=['GET'])
def services_tunnel(tunnel_key):

    response = ''

    # Checking if a tunnel_key was gived
    if tunnel_key is None or len(tunnel_key) != 36:
        return response

    # Fetching tunnel's informations for the requested tunnel key
    host = Host().get_one_by_tunnel_key(tunnel_key)

    if host:

        now = datetime.utcnow().strftime("%s")

        # Validating the remote address if authorized_ip specified in database
        if host.authorized_ip:

            remote_address = request.remote_addr

            if remote_address != host.authorized_ip:
                error = gettext(
                    'The remote IP address "%(remote_address)s" didn\'t match with the authorized IP', remote_address=remote_address)
                host.change(log=error, lastcheck=now)
                return response

        # Checking tunnel state...
        if host.tunnel_status == 'ON':
            if host.gateway_user and host.gateway_host and host.gateway_port:
                # Tunnel state ON
                host.change(log=gettext(
                    'The tunnel was ON, tunneling parameters has been send'), lastcheck=now)
                response = str(host.gateway_user) + ":" + \
                    str(host.gateway_host) + ":" + str(host.gateway_port)
            else:
                # Tunnel state ON but missing parameters
                msg = gettext(
                    'The tunnel was ON but a gateway parameter was missing')
                host.change(log=msg, lastcheck=now)
        else:
            # Tunnel state OFF
            host.change(log=gettext('The tunnel was OFF'), lastcheck=now)

    return Response(escape(response), 200)
